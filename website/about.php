<!doctype html>
<html lang="" class="page-about layout-01">
  
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SMW TECHNICALS</title>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- Icon font 7 Stroke -->
    <link rel="stylesheet" href="fonts/icon-7/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="fonts/icon-7/css/helper.css">

    <!-- Icon font Renovation -->
    <link rel="stylesheet" href="fonts/renovation/icon-font-renovation.css">

    <!-- Google font -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- Menu Mobile: mmenu -->
    <link type="text/css" rel="stylesheet" href="components/mmenu/jquery.mmenu.all.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="components/font-awesome/css/font-awesome.css" />

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="components/owl.carousel/assets/owl.carousel.css" />

    <!-- Light Gallery -->
    <link rel="stylesheet" href="components/lightgallery/css/lightgallery.css" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="components/bootstrap/css/bootstrap.css" />

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="components/slider/slider.css">

    <!-- THEME STYLE -->
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
  <div class="site">
    



<header class="site-header style-01">
  <div class="container">
    <div class="row row-xs-center">

      <div class="col-xs-10 col-lg-2 site-branding">
        <a href="index.php">
          <h4>SMW TECHNICALS</h4>
        </a>
      </div>

      <div class="col-xs-2 hidden-lg-up text-right">
        <a id="menu-button" href="#primary-menu-mobile"><i id="open-left" class="fa fa-bars"></i></a>
        <nav id="primary-menu-mobile">
          <ul>
            <li>
              <a href="index.php">HOME</a> 
            </li> 
            <li><a href="projects.php">projects</a> 
            </li>
            <li><a href="services.php">Services</a>
            </li>
            <li><a href="about.php">ABOUT</a> 
            </li>
            <li><a href="contact.php">Contact</a>
            </li>
			<li><a href="http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a>
            </li>
          </ul>
        </nav>
      </div>
      <!-- / Menu Mobile -->

      <div class="col-xs-12 col-sm-9 col-lg-8 extra-info">
        <div class="row">
          <div class="col-sm-5">
            <i class="fa fa-phone"></i>
            <div class="phone">
              <h3>+97 152 467 9252</h3>
              <span>mail@dubaipaintcompany.com</span>
            </div>
          </div>
          <div class="col-sm-7 col-md-6">
            <i class="fa fa-home"></i>
            <div class="address">
              <h3>Mallick Yaqub Building</h3>
              <span>Al Satwa, Dubai, UAE</span>
            </div>
          </div>
        </div>
      </div>
      <!-- /.extra-info -->

    </div>
  </div>

  <div class="social-menu social-menu_right-arrow hidden-md-down">
    <ul class="menu">
      <li class="menu-item"><a href="https://www.facebook.com/">facebook</a></li>
      <li class="menu-item"><a href="https://www.plus.google.com/">google+</a></li>
      <li class="menu-item"><a href="https://www.twitter.com/">twitter</a></li>
      <li class="menu-item"><a href="https://www.youtube.com/">youtube</a></li>
    </ul>
  </div>
  <!-- /.social-menu -->

</header>
<!-- /.site-header .style-01 -->

<nav id="primary-menu" class="primary-menu_style-01 hidden-md-down">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="menu">
          <li class="menu-item active">
            <a href="index.php">HOME</a> 
          </li>
          <li class="menu-item"><a href="projects.php">Projects</a> 
          </li>
          <li class="menu-item"><a href="services.php">Services</a>
              
          </li>
          <li class="menu-item"><a href="about.php">About</a> 
            </li>
          <li class="menu-item"><a href="contact.php">Contact</a>
          </li>
		  <li class="menu-item"><a href="http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a>
            </li>
        </ul>
      </div>
    </div>
  </div>
</nav>
<!-- /.primary-menu -->

			<div id="ssb-container" class="ssb-btns-right ssb-anim-slide" style="z-index: 1;right:-5px">
				<ul class="ssb-dark-hover">
					<li id="ssb-btn-0">
						<p style="color:#FFFFFF">
							<a href=""><img src="images/page-home/whatsapp.png"><i style="padding-top:20px;">+97 152 467 9252</i></a>
						</p>
					</li>
				</ul>
			</div>

<!-- Whatsapp conntact-->


<section class="about-big-title text-center" data-parallax="scroll" data-image-src="images/page-home/about_page_title_bg.jpg">
  <h2>We Provide Professional Services</h2>
  <p>TOTALLY AFFORADBLE AND EFFICIENT</p>
</section>
<!-- /.about-big-title -->

<div class="container">
  <div class="row about-slogan text-center">
    <div class="col-xs-12">
      <h3>“Getting the job done wherever, however, no matter how big or small.”</h3>
      <p>To set new standards of ethics and excellence in delivering to our customers superior quality and value-for-money residential and commercial spaces by employing a team of highly motivated and focused professionals.</p>
    </div>
  </div>
  <!-- /.about-slogan -->

  <div class="row home-maintenance">
    <div class="col-md-6">
      <h3>HOME Maintenance Business</h3>
      <p>One call does it all. Our company residential handyman services can help save time from the first phone call through project completion. Help with prioritizing major and minor home repairs and multi-tasking work means we complete jobs faster and better than your typical “handyman for hire”.</p>
      <p>Our Handyman team’s professionals arrive in nationally-recognized uniforms and logoed vans that are stocked with every tool they’ll need. You won’t have to interrupt your day, wasting valuable time waiting on us. Our on-time record is second to none, and we arrive ready to work. We are always ready to help you 24/7. For all your major and minor home repair needs, call us today to get the best offer.</p>
    </div>
    <div class="col-md-6">
      <img src="images/page-home/about_page_des.jpg" alt="">
    </div>
  </div>
  <!-- /.home-maintenance -->
</div>

<section class="feature-style-2" data-parallax="scroll" data-image-src="images/page-home/feature_style2_bg.jpg">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <div class="feature-item">
          <div class="feature-item_icon">
            <i class="pe-7s-tools"></i>
          </div>
          <h3 class="feature-item_title">
            Professional HandyMan
          </h3>
          <p class="feature-item_description">
            We are expert in Painting and Techinal Service.We offer you services that satisfies you & fullfil your needs.
          </p>
        </div>
      </div>

      <div class="col-sm-4">
        <div class="feature-item feature-item_clock">
          <div class="feature-item_icon">
            <i class="pe-7s-clock"></i>
          </div>
          <h3 class="feature-item_title">
            24/7 Services
          </h3>
          <p class="feature-item_description">
            If you are in emergency situation, please do not worry. We provide 24/7 service. Whenever you call, we service you.
          </p>
        </div>
      </div>

      <div class="col-sm-4">
        <div class="feature-item feature-item_price">
          <div class="feature-item_icon">
            <i class="pe-7s-cash"></i>
          </div>
          <h3 class="feature-item_title">
            Affordable Price
          </h3>
          <p class="feature-item_description">
            We do more than a handyman service- we check for glitches that need attention to keep you safe and save your money.
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.feature-style-2 -->

<section class="our-team our-team_style-1">
  <div class="text-center">
    <h2 class="heading-title">our team</h2>
    <p class="our-team_description">We have high quality handmen who are equiped with the latest tools.</p>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-6 our-team_item">
        <div class="our-team_item-img">
          <img src="images/page-home/team01-150x150.jpg" alt="team">
        </div>
        <div class="our-team_item-content">
          <h4 class="our-team_item-content-name">
            James Dean
          </h4>
          <p><strong>Heating, Electricity</strong></p>
          <p>James displayed a professional and trusting manner. His performance on the job was excellent. His major is heating and electricity.</p>
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>

      <div class="col-sm-6 our-team_item">
        <div class="our-team_item-img">
          <img src="images/page-home/team02-150x150.jpg" alt="team">
        </div>
        <div class="our-team_item-content">
          <h4 class="our-team_item-content-name">
            David Villar
          </h4>
          <p><strong>David Villar</strong></p>
          <p>James displayed a professional and trusting manner. His performance on the job was excellent. His major is heating and electricity.</p>
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>

      <div class="col-sm-6 our-team_item">
        <div class="our-team_item-img">
          <img src="images/page-home/team03-150x150.jpg" alt="team">
        </div>
        <div class="our-team_item-content">
          <h4 class="our-team_item-content-name">
            Arlo Reilly
          </h4>
          <p><strong>Painting</strong></p>
          <p>James displayed a professional and trusting manner. His performance on the job was excellent. His major is heating and electricity.</p>
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>

      <div class="col-sm-6 our-team_item">
        <div class="our-team_item-img">
          <img src="images/page-home/team04-150x150.jpg" alt="team">
        </div>
        <div class="our-team_item-content">
          <h4 class="our-team_item-content-name">
            Crawford Kerry
          </h4>
          <p><strong>Carpentry</strong></p>
          <p>James displayed a professional and trusting manner. His performance on the job was excellent. His major is heating and electricity.</p>
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>

      <div class="col-sm-6 our-team_item">
        <div class="our-team_item-img">
          <img src="images/page-home/team05-150x150.jpg" alt="team">
        </div>
        <div class="our-team_item-content">
          <h4 class="our-team_item-content-name">
            Jem Ernie
          </h4>
          <p><strong>Heating, Electricity</strong></p>
          <p>James displayed a professional and trusting manner. His performance on the job was excellent. His major is heating and electricity.</p>
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>

      <div class="col-sm-6 our-team_item">
        <div class="our-team_item-img">
          <img src="images/page-home/team07-150x150.jpg" alt="team">
        </div>
        <div class="our-team_item-content">
          <h4 class="our-team_item-content-name">
            Mort Bernie
          </h4>
          <p><strong>Remodeling</strong></p>
          <p>James displayed a professional and trusting manner. His performance on the job was excellent. His major is heating and electricity.</p>
          <ul>
            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.our-team -->

<section class="piece-of-us">
  <div class="container">
    <div class="row row-xs-center">
      <div class="col-md-1">
        <div class="piece-of-us_icon">
          <i class="fa fa-users"></i>
        </div>
      </div>
      <div class="col-md-8">

        <h2>Get in touch</h2>
        <p>Get in contact with us now to get our professional services on your door step.</p>
      </div>
      <div class="col-md-3">
        <a href="contact.php" class="btn">contact us now</a>
      </div>
    </div>
  </div>
</section>
<!-- /.piece-of-us -->


<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-about">
          <a href="index.php">
		  <h4 class="widget-tittle">SMW TECHNICALS</h4>
		  </a>
          <p>Our mission is to provide the best handyman service at an reasonable price without sacrificing quality. You will be satisfy with our work knowing we take the necessary steps to meet your needs and get the job done right</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-infomation">
          <h3 class="widget-tittle">infomation</h3>
          <ul class="menu-infomation">
            <li><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="projects.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
			<li><a href=" http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-info">
          <h3 class="widget-tittle">Our Office</h3>
          <ul>
            <li><i class="fa fa-map-marker"></i>Mallick Yaqub Building, Al Satwa, Dubai, UAE</li>
            <li><i class="fa fa-phone"></i>+97 152 467 9252</li>
            <li><i class="fa fa-envelope"></i>mail@dubaipaintcompany.com</li> 
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="social-menu social-menu_right-arrow">
    <ul class="menu">
      <li class="menu-item"><a href="https://www.facebook.com/">facebook</a></li>
      <li class="menu-item"><a href="https://www.plus.google.com/">google+</a></li>
      <li class="menu-item"><a href="https://www.twitter.com/">twitter</a></li>
      <li class="menu-item"><a href="https://www.youtube.com/">youtube</a></li>
    </ul>
  </div>
</footer>
<!-- /.footer -->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        Made with <i class="fa fa-heart"></i> by <a href="http://DevInSol.com">DevInSol</a>
      </div>
    </div>
  </div>
</div>
<!-- /.copyright -->


  </div>
	<!-- Map-About -->
	<script src="https://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>

    <script src="components/jquery/jquery.js"></script>
    <script src="components/bootstrap/js/bootstrap.js"></script>
    <script src="components/owl.carousel/owl.carousel.js"></script>
    <script src="components/parallax.js/parallax.js"></script>
  	<script src="components/scrollup/jquery.scrollUp.js"></script>
  	<script src="components/lightgallery/js/lightgallery.js"></script>

    <!-- Mobile Menu -->
    <script src="components/mmenu/jquery.mmenu.min.all.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script src="components/slider/jquery.themepunch.tools.min.js"></script>
    <script src="components/slider/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
    <script src="components/slider/extensions/revolution.extension.actions.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.carousel.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.migration.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.navigation.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.parallax.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.video.min.js"></script>

    <!-- ISOTOPE -->
    <script src="components/isotope.pkgd.min.js"></script>

    <!-- HOVER ISOTOPE -->
    <script src="components/jquery.directional-hover.min.js"></script>

    <!-- Image loaded ISOTOPE -->
    <script src="components/imagesloaded.pkgd.min.js"></script>

    <script src="js/main.js"></script>
  </body>

  </html>
