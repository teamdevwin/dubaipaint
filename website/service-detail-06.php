<!doctype html>
<html lang="" class="page-service-detail-01">
  
<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SMW TECHNICALS</title>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- Icon font 7 Stroke -->
    <link rel="stylesheet" href="fonts/icon-7/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="fonts/icon-7/css/helper.css">

    <!-- Icon font Renovation -->
    <link rel="stylesheet" href="fonts/renovation/icon-font-renovation.css">

    <!-- Google font -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- Menu Mobile: mmenu -->
    <link type="text/css" rel="stylesheet" href="components/mmenu/jquery.mmenu.all.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="components/font-awesome/css/font-awesome.css" />

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="components/owl.carousel/assets/owl.carousel.css" />

    <!-- Light Gallery -->
    <link rel="stylesheet" href="components/lightgallery/css/lightgallery.css" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="components/bootstrap/css/bootstrap.css" />

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="components/slider/slider.css">

    <!-- THEME STYLE -->
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
  <div class="site">
    

<!-- /.site-top .style-01 -->

<header class="site-header style-01">
  <div class="container">
    <div class="row row-xs-center">

      <div class="col-xs-10 col-lg-2 site-branding">
        <a href="index.php">
          <h4>SMW TECHNICALS</h4>
        </a>
      </div>

      <div class="col-xs-2 hidden-lg-up text-right">
        <a id="menu-button" href="#primary-menu-mobile"><i id="open-left" class="fa fa-bars"></i></a>
        <nav id="primary-menu-mobile">
          <ul>
            <li>
              <a href="index.php">HOME</a> 
            </li> 
            <li><a href="projects.php">projects</a> 
            </li>
            <li><a href="services.php">Services</a>
            </li>
            <li><a href="about.php">ABOUT</a> 
            </li>
            <li><a href="contact.php">Contact</a>
            </li>
			<li><a href="http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a>
            </li>
          </ul>
        </nav>
      </div>
      <!-- / Menu Mobile -->

      <div class="col-xs-12 col-sm-9 col-lg-8 extra-info">
        <div class="row">
          <div class="col-sm-5">
            <i class="fa fa-phone"></i>
            <div class="phone">
              <h3>+97 152 467 9252</h3>
              <span>mail@dubaipaintcompany.com</span>
            </div>
          </div>
          <div class="col-sm-7 col-md-6">
            <i class="fa fa-home"></i>
            <div class="address">
              <h3>Mallick Yaqub Building</h3>
              <span>Al Satwa, Dubai, UAE</span>
            </div>
          </div>
        </div>
      </div>
      <!-- /.extra-info -->

    </div>
  </div>

  <div class="social-menu social-menu_right-arrow hidden-md-down">
    <ul class="menu">
      <li class="menu-item"><a href="https://www.facebook.com/">facebook</a></li>
      <li class="menu-item"><a href="https://www.plus.google.com/">google+</a></li>
      <li class="menu-item"><a href="https://www.twitter.com/">twitter</a></li>
      <li class="menu-item"><a href="https://www.youtube.com/">youtube</a></li>
    </ul>
  </div>
  <!-- /.social-menu -->

</header>
<!-- /.site-header .style-01 -->

<nav id="primary-menu" class="primary-menu_style-01 hidden-md-down">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="menu">
          <li class="menu-item active">
            <a href="index.php">HOME</a> 
          </li>
          <li class="menu-item"><a href="projects.php">Projects</a> 
          </li>
          <li class="menu-item"><a href="services.php">Services</a>
              
          </li>
          <li class="menu-item"><a href="about.php">About</a> 
            </li>
          <li class="menu-item"><a href="contact.php">Contact</a>
          </li>
		  <li class="menu-item"><a href="http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a>
            </li>
        </ul>
      </div>
    </div>
  </div>
</nav>
<!-- /.primary-menu -->

			<div id="ssb-container" class="ssb-btns-right ssb-anim-slide" style="z-index: 1;right:-5px">
				<ul class="ssb-dark-hover">
					<li id="ssb-btn-0">
						<p style="color:#FFFFFF">
							<a href=""><img src="images/page-home/whatsapp.png"><i style="padding-top:20px;">+97 152 467 9252</i></a>
						</p>
					</li>
				</ul>
			</div>

<!-- Whatsapp conntact-->


<section class="big-title">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2>SMW TECHNICALS</h2>
      </div>
    </div>
  </div>
</section>
<!-- /.big-title -->


<div class="service-detail_img">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <img src="images/page-home/project02.jpg" alt="project02">
      </div>
      <div class="col-sm-4">
        <img src="images/page-home/project05.jpg" alt="project05">
      </div>
      <div class="col-sm-4">
        <img src="images/page-home/project06.jpg" alt="project06">
      </div>
    </div>
  </div>
</div>
<!-- /.service-detail_img -->

<div class="service-detail_content">
  <div class="container">
    <div class="row">
      <div class="col-sm-6">
        <h2 class="heading-title">SMW TECHNICALS</h2>
        <p>Our service specializes in Post Renovation Cleaning Services, Construction cleaning services, and Neglected property cleaning so we understand the importance of removing the dust and debris after a renovation/construction is completed, our company, is fully prepared to work within your time constraints; our attention to detail and the ability to work beyond your expectations, makes us the first and only choice, call us right now.</p>

        <h3 class="service-categories_title">We can help you with the following services:</h3>
        <ul class="service-list_item-categories">
          <li><a href="#">Plumbing & Sanitary Contracting</a></li>
          <li><a href="#">Floor and wall Tiling Works</a></li>
          <li><a href="#">Painting Contracting</a></li>
          <li><a href="#">Carpentry & Flooring Contracting</a></li>
          <li><a href="#">Engraving & Ornamentation Works</a></li>
          <li><a href="#">Wall Paper Fixing</a></li>
          <li><a href="#">Partitions & False Ceilings Contracting</a></li>
          <li><a href="#">Plaster & Cladding Works</a></li>
        </ul>
      </div>



<section class="piece-of-us">
  <div class="container">
    <div class="row row-xs-center">
      <div class="col-md-1">
        <div class="piece-of-us_icon">
          <i class="fa fa-users"></i>
        </div>
      </div>
      <div class="col-md-8">

        <h2>Get in touch</h2>
        <p>Get in contact with us now to get our professional services on your door step.</p>
      </div>
      <div class="col-md-3">
        <a href="contact.php" class="btn">contact us now</a>
      </div>
    </div>
  </div>
</section>
<!-- /.piece-of-us -->


<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-about">
          <a href="index.php">
		  <h4 class="widget-tittle">SMW TECHNICALS</h4>
		  </a>
          <p>Our mission is to provide the best handyman service at an reasonable price without sacrificing quality. You will be satisfy with our work knowing we take the necessary steps to meet your needs and get the job done right</p>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-infomation">
          <h3 class="widget-tittle">infomation</h3>
          <ul class="menu-infomation">
            <li><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="projects.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
			<li><a href=" http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-info">
          <h3 class="widget-tittle">Our Office</h3>
          <ul>
            <li><i class="fa fa-map-marker"></i>Mallick Yaqub Building, Al Satwa, Dubai, UAE</li>
            <li><i class="fa fa-phone"></i>+97 152 467 9252</li>
            <li><i class="fa fa-envelope"></i>mail@dubaipaintcompany.com</li> 
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="social-menu social-menu_right-arrow">
    <ul class="menu">
      <li class="menu-item"><a href="https://www.facebook.com/">facebook</a></li>
      <li class="menu-item"><a href="https://www.plus.google.com/">google+</a></li>
      <li class="menu-item"><a href="https://www.twitter.com/">twitter</a></li>
      <li class="menu-item"><a href="https://www.youtube.com/">youtube</a></li>
    </ul>
  </div>
</footer>
<!-- /.footer -->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        Made with <i class="fa fa-heart"></i> by <a href="http://DevInSol.com">DevInSol</a>
      </div>
    </div>
  </div>
</div>
<!-- /.copyright -->


  </div>
	<!-- Map-About -->
	<script src="https://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>

    <script src="components/jquery/jquery.js"></script>
    <script src="components/bootstrap/js/bootstrap.js"></script>
    <script src="components/owl.carousel/owl.carousel.js"></script>
    <script src="components/parallax.js/parallax.js"></script>
  	<script src="components/scrollup/jquery.scrollUp.js"></script>
  	<script src="components/lightgallery/js/lightgallery.js"></script>

    <!-- Mobile Menu -->
    <script src="components/mmenu/jquery.mmenu.min.all.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script src="components/slider/jquery.themepunch.tools.min.js"></script>
    <script src="components/slider/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
    <script src="components/slider/extensions/revolution.extension.actions.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.carousel.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.migration.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.navigation.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.parallax.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.video.min.js"></script>

    <!-- ISOTOPE -->
    <script src="components/isotope.pkgd.min.js"></script>

    <!-- HOVER ISOTOPE -->
    <script src="components/jquery.directional-hover.min.js"></script>

    <!-- Image loaded ISOTOPE -->
    <script src="components/imagesloaded.pkgd.min.js"></script>

    <script src="js/main.js"></script>
  </body>

  </html>
