<!doctype html>
<html lang="" class="page-home01">
  

<head>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SMW TECHNICALS</title>

    <link rel="apple-touch-icon" href="apple-touch-icon.png">

    <!-- Icon font 7 Stroke -->
    <link rel="stylesheet" href="fonts/icon-7/css/pe-icon-7-stroke.css">
    <link rel="stylesheet" href="fonts/icon-7/css/helper.css">

    <!-- Icon font Renovation -->
    <link rel="stylesheet" href="fonts/renovation/icon-font-renovation.css">

    <!-- Google font -->
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic' rel='stylesheet' type='text/css'>

    <!-- Menu Mobile: mmenu -->
    <link type="text/css" rel="stylesheet" href="components/mmenu/jquery.mmenu.all.css" />

    <!-- Font Awesome -->
    <link rel="stylesheet" href="components/font-awesome/css/font-awesome.css" />

    <!-- Owl Carousel -->
    <link rel="stylesheet" href="components/owl.carousel/assets/owl.carousel.css" />

    <!-- Light Gallery -->
    <link rel="stylesheet" href="components/lightgallery/css/lightgallery.css" />

    <!-- Bootstrap -->
    <link rel="stylesheet" href="components/bootstrap/css/bootstrap.css" />

    <!-- REVOLUTION STYLE SHEETS -->
    <link rel="stylesheet" type="text/css" href="components/slider/slider.css">

    <!-- THEME STYLE -->
    <link rel="stylesheet" href="css/main.css">

  </head>
  <body>
  <div class="site">
    
<!-- /.site-top .style-01 -->

<header class="site-header style-01">
  <div class="container">
    <div class="row row-xs-center">

      <div class="col-xs-10 col-lg-2 site-branding">
        <a href="index.php">
          <h4>SMW TECHNICALS</h4>
        </a>
      </div>

      <div class="col-xs-2 hidden-lg-up text-right">
        <a id="menu-button" href="#primary-menu-mobile"><i id="open-left" class="fa fa-bars"></i></a>
        <nav id="primary-menu-mobile">
          <ul>
            <li>
              <a href="index.php">HOME</a> 
            </li>
            <li><a href="projects.php">projects</a> 
            </li>
            <li><a href="services.php">Services</a>
            </li>
            <li><a href="about.php">ABOUT</a>
            </li>
            <li><a href="contact.php">Contact</a>
            </li>
			<li><a href="http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a>
            </li>
          </ul>
        </nav>
      </div>
      <!-- / Menu Mobile -->

      <div class="col-xs-12 col-sm-9 col-lg-8 extra-info">
        <div class="row">
          <div class="col-sm-5">
            <i class="fa fa-phone"></i>
            <div class="phone">
              <h3>+97 152 467 9252</h3>
              <span>mail@dubaipaintcompany.com</span>
            </div>
          </div>
          <div class="col-sm-7 col-md-6">
            <i class="fa fa-home"></i>
            <div class="address">
              <h3>Mallick Yaqub Building</h3>
              <span>Al Satwa, Dubai, UAE</span>
            </div>
          </div>
        </div>
      </div>
      <!-- /.extra-info -->

    </div>
  </div>

  <div class="social-menu social-menu_right-arrow hidden-md-down">
    <ul class="menu">
      <li class="menu-item"><a href="https://www.facebook.com/">facebook</a></li>
      <li class="menu-item"><a href="https://www.plus.google.com/">google+</a></li>
      <li class="menu-item"><a href="https://www.twitter.com/">twitter</a></li>
      <li class="menu-item"><a href="https://www.youtube.com/">youtube</a></li>
    </ul>
  </div>
  <!-- /.social-menu -->

</header>
<!-- /.site-header .style-01 -->

<nav id="primary-menu" class="primary-menu_style-01 hidden-md-down">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <ul class="menu">
          <li class="menu-item active">
            <a href="index.php">HOME</a> 
          </li>
          <li class="menu-item"><a href="projects.php">Projects</a> 
          </li>
          <li class="menu-item"><a href="services.php">Services</a>
              
          </li>
          <li class="menu-item"><a href="about.php">About</a> 
            </li>
          <li class="menu-item"><a href="contact.php">Contact</a>
          </li>
		  <li class="menu-item"><a href="http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a>
            </li>
        </ul>
      </div>
    </div>
  </div>
</nav>
<!-- /.primary-menu -->

			<div id="ssb-container" class="ssb-btns-right ssb-anim-slide" style="z-index: 1;right:-5px">
				<ul class="ssb-dark-hover">
					<li id="ssb-btn-0">
						<p style="color:#FFFFFF">
							<a href=""><img src="images/page-home/whatsapp.png"><i style="padding-top:20px;">+97 152 467 9252</i></a>
						</p>
					</li>
				</ul>
			</div>

<!-- Whatsapp conntact-->

<div id="rev_slider_1_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="homepage-01" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
  <!-- START REVOLUTION SLIDER 5.0.9 auto mode -->
  <div id="rev_slider_1_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.9">
    <ul>	<!-- SLIDE  -->
      <li data-index="rs-1" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://localhost/wp-renovation-demo/wp-content/"  data-delay="9030"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 1" data-description="">
        <!-- MAIN IMAGE -->
        <img src="images/page-home/home01-slide01.jpg" style='background-color:#666666' alt=""  data-lazyload="images/page-home/home01-slide01.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="1" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-1"
             id="slide-1-layer-1"
             data-x="['left','left','left','left']" data-hoffset="['1100','1100','1100','1100']"
             data-y="['top','top','top','top']" data-voffset="['283','283','283','283']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="x:[-105%];z:0;rX:0deg;rY:0deg;rZ:-90deg;sX:1;sY:1;skX:0;skY:0;s:200;e:Power4.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
             data-start="500"
             data-splitin="chars"
             data-splitout="none"
             data-responsive_offset="on"

             data-elementdelay="0.1"
             data-end="9000"

             style="z-index: 5; white-space: nowrap; color: rgba(252, 210, 7, 1.00);">PROFESSIONAL
        </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-2"
             id="slide-1-layer-3"
             data-x="['left','left','left','left']" data-hoffset="['960','960','960','960']"
             data-y="['top','top','top','top']" data-voffset="['340','340','340','340']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="880"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"

             data-end="9000"

             style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);">PAINTING & TECHNICAL SERVICES
        </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-1-layer-4"
             data-x="['left','left','left','left']" data-hoffset="['1257','1257','1257','1257']"
             data-y="['top','top','top','top']" data-voffset="['207','207','207','207']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="880"
             data-responsive_offset="on"

             data-end="9000"

             style="z-index: 7;"><img src="images/page-home/home01-slide01-obj4.png" alt="" data-ww="['61px','61px','61px','61px']" data-hh="['52px','52px','52px','52px']" data-lazyload="images/page-home/home01-slide01-obj4.png" data-no-retina>
        </div>

        <!-- LAYER NR. 4 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-1-layer-7"
             data-x="['left','left','left','left']" data-hoffset="['222','222','222','222']"
             data-y="['top','top','top','top']" data-voffset="['161','161','161','161']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="1030"
             data-responsive_offset="on"

             data-end="9000"

             style="z-index: 8;"><img src="images/page-home/home01-slide01-obj3.png" alt="" data-ww="['383px','383px','383px','383px']" data-hh="['494px','494px','494px','494px']" data-lazyload="images/page-home/home01-slide01-obj3.png" data-no-retina>
        </div>

        <!-- LAYER NR. 5 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-1-layer-8"
             data-x="['left','left','left','left']" data-hoffset="['646','646','646','646']"
             data-y="['top','top','top','top']" data-voffset="['150','150','150','150']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="800"
             data-responsive_offset="on"

             data-end="9000"

             style="z-index: 9;"><img src="images/page-home/home01-slide01-obj2.png" alt="" data-ww="['314px','314px','314px','314px']" data-hh="['505px','505px','505px','505px']" data-lazyload="images/page-home/home01-slide01-obj2.png" data-no-retina>
        </div>

        <!-- LAYER NR. 6 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-1-layer-5"
             data-x="['left','left','left','left']" data-hoffset="['439','439','439','439']"
             data-y="['top','top','top','top']" data-voffset="['93','93','93','93']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:1540;e:Power3.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="700"
             data-responsive_offset="on"

             data-end="9000"

             style="z-index: 10;"><img src="images/page-home/home01-slide01-obj1.png" alt="" data-ww="['445px','445px','445px','445px']" data-hh="['562px','562px','562px','562px']" data-lazyload="images/page-home/home01-slide01-obj1.png" data-no-retina>
        </div>

        <!-- LAYER NR. 7 -->
        <div class="tp-caption Renovation-Button rev-btn  rs-parallaxlevel-0"
             id="slide-1-layer-10"
             data-x="['left','left','left','left']" data-hoffset="['1197','1197','1197','1197']"
             data-y="['top','top','top','top']" data-voffset="['422','422','422','422']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"
             data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;"
             data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(17, 17, 17, 1.00);"

             data-transform_in="opacity:0;s:300;e:Power2.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="880"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"
             data-responsive="off"
             data-end="9000"

             style="z-index: 11; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="contact.php">CONTACT NOW</a> <i class="fa-icon-angle-double-right"></i>
        </div>
      </li>
      <!-- SLIDE  -->
      <li data-index="rs-2" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://localhost/wp-renovation-demo/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 2" data-description="">
        <!-- MAIN IMAGE -->
        <img src="images/page-home/home01-slide02.jpg" style='background-color:#636363' alt=""  data-lazyload="images/page-home/home01-slide02.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-1"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['142','142','142','142']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
             data-start="800"
             data-responsive_offset="on"


             style="z-index: 5;"><img src="images/page-home/home01-slide02-obj1.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']" data-lazyload="images/page-home/home01-slide02-obj1.png" data-no-retina>
        </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-2"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['273','273','273','273']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="500"
             data-splitin="chars"
             data-splitout="none"
             data-responsive_offset="on"

             data-elementdelay="0.05"

             style="z-index: 6; white-space: nowrap;">WE CAN DO ANYTHING!
        </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption Renovation-Button rev-btn  rs-parallaxlevel-2"
             id="slide-2-layer-3"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['370','370','370','370']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"
             data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;"
             data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(17, 17, 17, 1.00);"

             data-transform_in="opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="600"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"
             data-responsive="off"

             style="z-index: 7; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="contact.php">CONTACT NOW</a>  <i class="fa-icon-angle-double-right"></i>
        </div>
      </li>
      <!-- SLIDE  -->
      <li data-index="rs-3" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://localhost/wp-renovation-demo/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 3" data-description="">
        <!-- MAIN IMAGE -->
        <img src="images/page-home/home01-slide03.jpg" style='background-color:#515151' alt=""  data-lazyload="images/page-home/home01-slide03.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-1"
             id="slide-3-layer-1"
             data-x="['left','left','left','left']" data-hoffset="['680','680','680','680']"
             data-y="['top','top','top','top']" data-voffset="['220','220','220','220']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="x:[175%];y:0px;z:0;rX:0;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0.01;s:1500;e:Power3.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-mask_in="x:[-100%];y:0;s:inherit;e:inherit;"
             data-start="500"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"


             style="z-index: 5; white-space: nowrap; color: rgba(251, 210, 50, 1.00);">PROFESSIONAL PAINTING SERVICES
        </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-2"
             id="slide-3-layer-2"
             data-x="['left','left','left','left']" data-hoffset="['680','680','680','680']"
             data-y="['top','top','top','top']" data-voffset="['280','280','280','280']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="x:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
             data-start="500"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"


             style="z-index: 6; white-space: nowrap; color: rgba(255, 255, 255, 1.00);">HOUSE, VILLA & APPARTMENT PAINTING
        </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-3-layer-3"
             data-x="['left','left','left','left']" data-hoffset="['545','545','545','545']"
             data-y="['top','top','top','top']" data-voffset="['208','208','208','208']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="600"
             data-responsive_offset="on"


             style="z-index: 7;"><img src="images/page-home/home01-slide03-obj1.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['94px','94px','94px','94px']" data-lazyload="images/page-home/home01-slide03-obj1.png" data-no-retina>
        </div>

        <!-- LAYER NR. 4 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-3-layer-4"
             data-x="['left','left','left','left']" data-hoffset="['546','546','546','546']"
             data-y="['top','top','top','top']" data-voffset="['305','305','305','305']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="700"
             data-responsive_offset="on"


             style="z-index: 8;"><img src="images/page-home/home01-slide03-obj3.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['94px','94px','94px','94px']" data-lazyload="images/page-home/home01-slide03-obj3.png" data-no-retina>
        </div>

        <!-- LAYER NR. 5 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-3-layer-5"
             data-x="['left','left','left','left']" data-hoffset="['460','460','460','460']"
             data-y="['top','top','top','top']" data-voffset="['257','257','257','257']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="800"
             data-responsive_offset="on"


             style="z-index: 9;"><img src="images/page-home/home01-slide03-obj2.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['94px','94px','94px','94px']" data-lazyload="images/page-home/home01-slide03-obj2.png" data-no-retina>
        </div>

        <!-- LAYER NR. 6 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-3-layer-7"
             data-x="['left','left','left','left']" data-hoffset="['461','461','461','461']"
             data-y="['top','top','top','top']" data-voffset="['354','354','354','354']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="1000"
             data-responsive_offset="on"


             style="z-index: 10;"><img src="images/page-home/home01-slide03-obj5.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['94px','94px','94px','94px']" data-lazyload="images/page-home/home01-slide03-obj5.png" data-no-retina>
        </div>

        <!-- LAYER NR. 7 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-0"
             id="slide-3-layer-8"
             data-x="['left','left','left','left']" data-hoffset="['375','375','375','375']"
             data-y="['top','top','top','top']" data-voffset="['306','306','306','306']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="1100"
             data-responsive_offset="on"


             style="z-index: 11;"><img src="images/page-home/home01-slide03-obj4.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['94px','94px','94px','94px']" data-lazyload="images/page-home/home01-slide03-obj4.png" data-no-retina>
        </div>

        <!-- LAYER NR. 8 -->
        <div class="tp-caption Renovation-Button rev-btn  rs-parallaxlevel-1"
             id="slide-3-layer-10"
             data-x="['left','left','left','left']" data-hoffset="['680','680','680','680']"
             data-y="['top','top','top','top']" data-voffset="['350','350','350','350']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"
             data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;"
             data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(17, 17, 17, 1.00);"

             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="1200"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"
             data-responsive="off"

             style="z-index: 12; white-space: nowrap;background-color:rgba(251, 210, 50, 1.00);outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="contact.php">CONTACT NOW</a>  <i class="fa-icon-angle-double-right"></i>
        </div>
      </li>
	  <li data-index="rs-4" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://localhost/wp-renovation-demo/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 4" data-description="">
        <!-- MAIN IMAGE -->
        <img src="images/page-home/home01-slide04.jpg" style='background-color:#636363' alt=""  data-lazyload="images/page-home/home01-slide04.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-1"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['142','142','142','142']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
             data-start="800"
             data-responsive_offset="on"


             style="z-index: 5;"><img src="images/page-home/home01-slide02-obj1.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']" data-lazyload="images/page-home/home01-slide02-obj1.png" data-no-retina>
        </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-2"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['273','273','273','273']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="500"
             data-splitin="chars"
             data-splitout="none"
             data-responsive_offset="on"

             data-elementdelay="0.05"

             style="z-index: 6; white-space: nowrap;">VILLA PAINTING
        </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption Renovation-Button rev-btn  rs-parallaxlevel-2"
             id="slide-2-layer-3"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['370','370','370','370']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"
             data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;"
             data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(17, 17, 17, 1.00);"

             data-transform_in="opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="600"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"
             data-responsive="off"

             style="z-index: 7; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="contact.php">CONTACT NOW</a>  <i class="fa-icon-angle-double-right"></i>
        </div>
      </li>
	   <li data-index="rs-5" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://localhost/wp-renovation-demo/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 5" data-description="">
        <!-- MAIN IMAGE -->
        <img src="images/page-home/home01-slide05.jpg" style='background-color:#636363' alt=""  data-lazyload="images/page-home/home01-slide05.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-1"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['142','142','142','142']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
             data-start="800"
             data-responsive_offset="on"


             style="z-index: 5;"><img src="images/page-home/home01-slide02-obj1.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']" data-lazyload="images/page-home/home01-slide02-obj1.png" data-no-retina>
        </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-2"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['273','273','273','273']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="500"
             data-splitin="chars"
             data-splitout="none"
             data-responsive_offset="on"

             data-elementdelay="0.05"

             style="z-index: 6; white-space: nowrap;">HOUSE PAINTING
        </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption Renovation-Button rev-btn  rs-parallaxlevel-2"
             id="slide-2-layer-3"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['370','370','370','370']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"
             data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;"
             data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(17, 17, 17, 1.00);"

             data-transform_in="opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="600"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"
             data-responsive="off"

             style="z-index: 7; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="contact.php">CONTACT NOW</a>  <i class="fa-icon-angle-double-right"></i>
        </div>
      </li>
	  <li data-index="rs-6" data-transition="fade" data-slotamount="default"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-thumb="http://localhost/wp-renovation-demo/wp-content/"  data-rotate="0"  data-saveperformance="off"  data-title="Slide 6" data-description="">
        <!-- MAIN IMAGE -->
        <img src="images/page-home/home01-slide06.jpg" style='background-color:#636363' alt=""  data-lazyload="images/page-home/home01-slide06.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
        <!-- LAYERS -->

        <!-- LAYER NR. 1 -->
        <div class="tp-caption   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-1"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['142','142','142','142']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
             data-start="800"
             data-responsive_offset="on"


             style="z-index: 5;"><img src="images/page-home/home01-slide02-obj1.png" alt="" data-ww="['110px','110px','110px','110px']" data-hh="['95px','95px','95px','95px']" data-lazyload="images/page-home/home01-slide02-obj1.png" data-no-retina>
        </div>

        <!-- LAYER NR. 2 -->
        <div class="tp-caption Renovation-BigTitle   tp-resizeme rs-parallaxlevel-1"
             id="slide-2-layer-2"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['273','273','273','273']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"

             data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="500"
             data-splitin="chars"
             data-splitout="none"
             data-responsive_offset="on"

             data-elementdelay="0.05"

             style="z-index: 6; white-space: nowrap;">OFFICE PAINTING
        </div>

        <!-- LAYER NR. 3 -->
        <div class="tp-caption Renovation-Button rev-btn  rs-parallaxlevel-2"
             id="slide-2-layer-3"
             data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
             data-y="['top','top','top','top']" data-voffset="['370','370','370','370']"
             data-width="none"
             data-height="none"
             data-whitespace="nowrap"
             data-transform_idle="o:1;"
             data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:0;e:Linear.easeNone;"
             data-style_hover="c:rgba(255, 255, 255, 1.00);bg:rgba(17, 17, 17, 1.00);"

             data-transform_in="opacity:0;s:1000;e:Power2.easeOut;"
             data-transform_out="opacity:0;s:300;s:300;"
             data-start="600"
             data-splitin="none"
             data-splitout="none"
             data-responsive_offset="on"
             data-responsive="off"

             style="z-index: 7; white-space: nowrap;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;"><a href="contact.php">CONTACT NOW</a>  <i class="fa-icon-angle-double-right"></i>
        </div>
      </li>
    </ul>
    <div class="tp-static-layers">
    </div>
    <div class="tp-bannertimer" style="height: 5px; background-color: rgba(0, 0, 0, 0.15);"></div>	</div>
</div><!-- END REVOLUTION SLIDER -->



<section class="features-style-1">
  <div class="container">
    <div class="row">

      <div class="col-sm-12">
        <h2 class="heading-title">Our Features</h2>
        <p class="description">
          From complete turn key to project manager. Leave the building to the professionals.
        </p>

      </div>

      <div class="col-sm-12 col-md-4">
        <div class="feature-item">
          <div class="feature-item-wrapper">
            <h3 class="feature-item_title">We Are Expert</h3>
            <p>We are expert in Painting and</br>Techinal Service.We offer you</br>services that satisfies you & fullfil</br>your needs.</p>
            <i class="pe-7s-tools"></i>
          </div>
        </div>
      </div>

      <div class="col-sm-12 col-md-4">
        <div class="feature-item">
          <div class="feature-item-wrapper">
            <h3 class="feature-item_title">24/7 Services</h3>
            <p>If you are in emergency situation, please do not worry. We provide 24/7 service. Whenever you call, we service you.</p>
            <i class="pe-7s-clock"></i>
          </div>
        </div>
      </div>

      <div class="col-sm-12 col-md-4">
        <div class="feature-item">
          <div class="feature-item-wrapper">
            <h3 class="feature-item_title">Affordable Price</h3>
            <p>We do more than a handyman service- we check for glitches that need attention to keep you safe and save your money.</p>
            <i class="pe-7s-cash"></i>
          </div>
        </div>
      </div>

    </div>
  </div>
</section>
<!-- /.features-style-1 -->

<section class="we-are we-are-layout1" data-parallax="scroll" data-image-src="images/page-home/home01_projects_bg.jpg">
  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-lg-6">
        <h2 class="heading-title">We Are Professional & Thoughful Service Providers</h2>
        <div class="description">
          <p>SMW Technical Service LLC was established in response to the growing need for quality housing and commercial space in the city. Since then, we have grown to be one of the leading real estate developers serving the needs of a discerning clientele.</p>
          <ul>
            <li>We have high quality labor who are trainned every year with latest technology.</li>
            <li>We have 5 year experience in building market.</li>
            <li>Material from the top partner in market.</li>
            <li>Update the tendency from around the world.</li>
          </ul>
          <a href="projects.php" class="btn">view our projects <i class="fa fa-angle-double-right"></i></a>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.we-are-layout1 -->

<section class="services services-style-01">
  <div class="service-heading">
    <div class="container">
      <div class="row">
        <div class="col-sm-12">
          <h2 class="heading-title">Services</h2>
          <p class="description">From complete turn key to project manager. Leave the building to the professionals.</p>
        </div>
      </div>
    </div>
  </div>
  <div class="service-content">
    <div class="container">
      <div class="row">

        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="service-item_img">
              <a href="service-detail-01.php"><img src="images/page-home/services-03.jpg" alt="service-renovation"></a>
            </div>
            <a class="service-item_link" href="service-detail-01.php">
              <div class="service-item_icon"><i class="rn-renovation"></i></div>
              Painting
            </a>
          </div>
        </div>

        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="service-item_img">
              <a href="service-detail-02.php"><img src="images/page-home/services-02.jpg" alt="service-renovation"></a>
            </div>
            <a class="service-item_link" href="service-detail-02.php">
              <div class="service-item_icon"><i class="rn-electrical"></i></div>
              Tiling
            </a>
          </div>
        </div>

        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="service-item_img">
              <a href="service-detail-03.php"><img src="images/page-home/services-04.jpg" alt="service-renovation"></a>
            </div>
            <a class="service-item_link" href="service-detail-03.php">
              <div class="service-item_icon"><i class="rn-painting"></i></div>
              Plumbing
            </a>
          </div>
        </div>

        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="service-item_img">
              <a href="service-detail-04.php"><img src="images/page-home/services-01.jpg" alt="service-renovation"></a>
            </div>
            <a class="service-item_link" href="service-detail-04.php">
              <div class="service-item_icon"><i class="rn-plumbing"></i></div>
              Carpentry
            </a>
          </div>
        </div>

        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="service-item_img">
              <a href="service-detail-05.php"><img src="images/page-home/services-05.jpg" alt="service-renovation"></a>
            </div>
            <a class="service-item_link" href="service-detail-05.php">
              <div class="service-item_icon"><i class="rn-heating"></i></div>
              Ornamentation
            </a>
          </div>
        </div>

        <div class="col-sm-6 col-md-4">
          <div class="service-item">
            <div class="service-item_img">
              <a href="service-detail-06.php"><img src="images/page-home/services-06.jpg" alt="service-renovation"></a>
            </div>
            <a class="service-item_link" href="service-detail-06.php">
              <div class="service-item_icon"><i class="rn-home"></i></div>
              Fixing
            </a>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
<!-- /.services-style-01 -->

<section class="our-works" data-parallax="scroll" data-image-src="images/page-home/home01_our_work_bg.jpg">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="heading-title">Our works</h2>
        <p>We let our quality work and commitment to customer satisfaction be our slogan.</p>

        <div class="button-group filters-button-group">
          <button class="button is-checked" data-filter="*">All work</button>
          <button class="button" data-filter=".interior">interior</button>
          <button class="button" data-filter=".painting">painting</button>
          <button class="button" data-filter=".plumbing">plumbing</button>
        </div>
      </div>
    </div>
  </div>

    <div class="grid">
      <div class="element-item plumbing item-dir" data-category="plumbing">
        <img src="images/page-home/project01.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Plumbing</a>
            <a class="grid-overlay-tittle" href="projects-single-fullwidth.php">Bathroom Plumbing</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item painting item-dir" data-category="painting">
        <img src="images/page-home/project02.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Painting</a>
            <a class="grid-overlay-tittle" href="#">Plaster & Cladding</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item interior item-dir" data-category="interior">
        <img src="images/page-home/project03.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Interior</a>
            <a class="grid-overlay-tittle" href="#">Door & Wall Painting</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item painting item-dir" data-category="painting">
        <img src="images/page-home/project04.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Painting</a>
            <a class="grid-overlay-tittle" href="#">Kitchen Painting</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item interior painting item-dir" data-category="interior">
        <img src="images/page-home/project05.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Interior, Painting</a>
            <a class="grid-overlay-tittle" href="#">Kitchen Remodel</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item repair item-dir" data-category="repair">
        <img src="images/page-home/project06.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Carpentry</a>
            <a class="grid-overlay-tittle" href="#">House & Office Carpentry</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item painting item-dir" data-category="painting">
        <img src="images/page-home/project07.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Painting</a>
            <a class="grid-overlay-tittle" href="#">Blue Working Room</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item electrical item-dir" data-category="electrical">
        <img src="images/page-home/project08.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Painting</a>
            <a class="grid-overlay-tittle" href="#">Roof Painting</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item repair item-dir" data-category="repair">
        <img src="images/page-home/project09.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Fixing</a>
            <a class="grid-overlay-tittle" href="#">Wall Paper Fixing</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
      <div class="element-item electrical item-dir" data-category="electrical">
        <img src="images/page-home/project10.jpg" alt="projects">
        <div class="grid-overlay">
          <div class="grid-overlay-wrapper">
            <a class="grid-overlay-category" href="#">Fixing</a>
            <a class="grid-overlay-tittle" href="#">Wall Fixing</a>
            <a class="grid-overlay-icon" href="#"><i class="fa fa-search"></i></a>
          </div>
        </div>
      </div>
    </div>


</section>
<!-- /.our-works -->

<section class="testimonial">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <h2 class="heading-title">Testimonial</h2>
        <p class="description">Creating a sustainable future through building preservation, green architecture, and smart design.</p>

        <div class="testi-owl owl-carousel">
          <div class="quote">
            <blockquote>
              <p>Technician came over to do a few odd&nbsp;jobs&nbsp;and did great work. Fixed a window, a sink and an electrical plug all for a reasonable price. I satisfy with his work and your service. Would definitely use them again.</p>
            </blockquote>
            <a href="#" class="avatar-link">
              <img src="images/page-home/testimonial01.jpg" alt="testimonial 01">
            </a>
            <cite class="author">
              <span class="author-name">Frankie Kao</span>
            </cite>
          </div>
          <!-- /.quote -->

          <div class="quote">
            <blockquote>
              <p>I have used the services of your company on a few occasions. Your crews are always <strong>personable, prompt and efficient</strong>. The work performed has been <strong>very good and as described</strong>. I have confidence in recommending their services to anyone.</p>
            </blockquote>
            <a href="#" class="avatar-link">
              <img src="images/page-home/testimonial02.jpg" alt="testimonial 02">
            </a>
            <cite class="author">
              <span class="author-name">Michael F. Schroeder</span>
            </cite>
          </div>
          <!-- /.quote -->

          <div class="quote">
            <blockquote>
              <p>Your crews work more than expected. Even I call at early morning or at night they come and do the work prompt and effective. They are friendly and has a careful style of work. I am satisfy and recommend other to use your service.</p>
            </blockquote>
            <a href="#" class="avatar-link">
              <img src="images/page-home/testimonial03.jpg" alt="testimonial 03">
            </a>
            <cite class="author">
              <span class="author-name">Ms. Marry Jane</span>
            </cite>
          </div>
          <!-- /.quote -->

        </div>
      </div>
    </div>
  </div>
</section>
<!-- /.testimonial -->

<section class="contact-form" data-parallax="scroll" data-image-src="images/page-home/home01_contact_form_bg.jpg">
  <div class="container">
    <div class="row">

      <div class="col-sm-12 col-md-6 col-md-offset-6">
        <h2 class="heading-title">Get a quick quote</h2>
        <p>Send us an email by entering your details and any comments you may have in the form.</p>

        <form action="#" method="POST">
          <div class="row">
            <div class="col-sm-6">
              <input type="text" name="your-name" required="required" placeholder="Your Name">
            </div>
            <div class="col-sm-6">
              <input type="email" name="your-email" required="required" placeholder="Your Email">
            </div>
            <div class="col-sm-6">
              <input type="text" name="your-subject" value="" size="40" aria-required="true" aria-invalid="false" placeholder="Subject">
            </div>
            <div class="col-sm-6">
              <input type="text" name="your-phone" value="" size="40" aria-invalid="false" placeholder="Phone">
            </div>
            <div class="col-sm-12">
              <textarea name="your-message" cols="40" rows="10" aria-invalid="false" placeholder="Your message"></textarea>
            </div>
            <div class="col-sm-12">
              <input type="submit" value="Send Message" class="btn">
            </div>
          </div>
        </form>
		<?php
					 if (isset($_POST["your-name"]) && isset($_POST["your-phone"]) && isset($_POST["your-email"]) && isset($_POST["your-message"]) && isset($_POST["your-subject"])){
					  $msg = "Name: " .$_POST["your-name"] ."\n"
					   ."Email: " .$_POST["your-email"] ."\n"
					   ."Phone: " .$_POST["your-phone"] ."\n"
					   ."Message: " .$_POST["your-message"]."\n"
					   ."Subject: " .$_POST["your-subject"];
					   @mail("mail@dubaipaintcompany.com", "new order", $msg);
					   echo "Email send thanks for contact!";
					   header("location: multumim.php");
					 } else if (empty($_POST["your-name"]) && empty($_POST["your-email"])&&empty($_POST["your-message"])){
					  $msg = "Name: " .$_POST["your-name"] ."\n"
					   ."Email: " .$_POST["your-email"] ."\n"
					   ."Phone: " .$_POST["your-phone"] ."\n"
					   ."Message: " .$_POST["your-message"];
					   @mail("its-better-not-to-show-it-but-its-valid", "new order", $msg);
					   header("location: multumim.php");
					 }else{
					  header("location: comanda-magneti.php");
					  exit(0);
					 }
					?>

      </div>

    </div>
  </div>
</section>
<!-- #contact-form -->
</br>
</br>
</br>
</br>
</br>
</br>



<footer class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-about">
          <!--<a href="index.php">
		  <h4 class="widget-tittle">SMW TECHNICALS</h4>
		  </a>-->
		  <a href="index.php">
		  <h4 class="widget-tittle">SMW TECHNICALS</h4>
		  </a>
          <p>Our mission is to provide the best handyman service at an reasonable price without sacrificing quality. You will be satisfy with our work knowing we take the necessary steps to meet your needs and get the job done right</p>
        </div>
      </div>
      <div class="col-md-4"> 
        <div class="footer-infomation">
          <h3 class="widget-tittle">infomation</h3>
          <ul class="menu-infomation">
            <li><a href="services.php">Services</a></li>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="projects.php">Projects</a></li>
            <li><a href="about.php">About</a></li>
			<li><a href=" http://www.jotun.com/me/en/b2c/inspiration/colours-gallery/index.aspx">Jotun</a></li>
          </ul>
        </div>
      </div>
      <div class="col-md-4">
        <div class="footer-info">
          <h3 class="widget-tittle">Our Office</h3>
          <ul>
            <li><i class="fa fa-map-marker"></i>Mallick Yaqub Building, Al Satwa, Dubai, UAE</li>
            <li><i class="fa fa-phone"></i>+97 152 467 9252</li>
            <li><i class="fa fa-envelope"></i>mail@dubaipaintcompany.com</li> 
          </ul>
        </div>
      </div>
    </div>
  </div>
  <div class="social-menu social-menu_right-arrow">
    <ul class="menu">
      <li class="menu-item"><a href="https://www.facebook.com/">facebook</a></li>
      <li class="menu-item"><a href="https://www.plus.google.com/">google+</a></li>
      <li class="menu-item"><a href="https://www.twitter.com/">twitter</a></li>
      <li class="menu-item"><a href="https://www.youtube.com/">youtube</a></li>
    </ul>
  </div>
</footer>
<!-- /.footer -->

<div class="copyright">
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        Made with <i class="fa fa-heart"></i> by <a href="http://DevInSol.com">DevInSol</a>
      </div>
    </div>
  </div>
</div>
<!-- /.copyright -->


  </div>
	<!-- Map-About -->
	<script src="https://maps.google.com/maps/api/js?sensor=false&amp;language=en"></script>

    <script src="components/jquery/jquery.js"></script>
    <script src="components/bootstrap/js/bootstrap.js"></script>
    <script src="components/owl.carousel/owl.carousel.js"></script>
    <script src="components/parallax.js/parallax.js"></script>
  	<script src="components/scrollup/jquery.scrollUp.js"></script>
  	<script src="components/lightgallery/js/lightgallery.js"></script>

    <!-- Mobile Menu -->
    <script src="components/mmenu/jquery.mmenu.min.all.js"></script>

    <!-- REVOLUTION JS FILES -->
    <script src="components/slider/jquery.themepunch.tools.min.js"></script>
    <script src="components/slider/jquery.themepunch.revolution.min.js"></script>

    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS -->
    <script src="components/slider/extensions/revolution.extension.actions.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.carousel.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.migration.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.navigation.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.parallax.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="components/slider/extensions/revolution.extension.video.min.js"></script>

    <!-- ISOTOPE -->
    <script src="components/isotope.pkgd.min.js"></script>

    <!-- HOVER ISOTOPE -->
    <script src="components/jquery.directional-hover.min.js"></script>

    <!-- Image loaded ISOTOPE -->
    <script src="components/imagesloaded.pkgd.min.js"></script>

    <script src="js/main.js"></script>
  </body>


  </html>
